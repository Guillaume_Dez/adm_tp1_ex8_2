from setuptools import setup

setup(
    name='TestSimpleCalculator',
    version='0.0.1',
    packages=['calculator',],
    license='GNU GPLv3',
)
