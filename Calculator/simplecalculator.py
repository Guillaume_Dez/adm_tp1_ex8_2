#!/usr/bin/env python
# coding: utf-8
'''
Création de la classe SimpleCalculator
'''
class SimpleCalculator:
    '''
        This is a class for mathematical operations.

        Attributes:
            varible_un (int): La première variable.
            variable_deux (int): La deuxième variable.
    '''
    result = 0
    @staticmethod
    def ajout(variable_un, variable_deux):
        '''return l'ajout'''
        if isinstance(variable_un,int) and isinstance(int,variable_deux):
            return variable_un + variable_deux
        else:
            return "ERROR"

    @staticmethod
    def soust(variable_un, variable_deux):
        '''return la soustraction'''
        if isinstance(variable_un,int) and isinstance(int,variable_deux):
            return variable_un - variable_deux
        else:
            return "ERROR"
    @staticmethod
    def mul(variable_un, variable_deux):
        '''return la multiplication'''
        if isinstance(variable_un,int) and isinstance(int,variable_deux):
            return variable_un * variable_deux
        else:
            return "ERROR"
    @staticmethod
    def div(variable_un, variable_deux):
        '''return la division'''
        if isinstance(variable_un,int) and isinstance(int,variable_deux):
            if variable_deux == 0:
                raise ZeroDivisionError("Division par Zero")
            else:
                return variable_un / variable_deux
        else:
            return "ERROR"


###TEST

if __name__ == "__main__":
    '''
        TESTS
    '''

VAR_1 = 2
VAR_2 = 10

S = SimpleCalculator()
print("test :")
print(S.ajout(VAR_1, VAR_2))
print(S.soust(VAR_1, VAR_2))
