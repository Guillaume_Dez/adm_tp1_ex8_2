#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import logging

from calculator.simplecalculator import SimpleCalculator as SimpleCalculator

class TestAjout(unittest.TestCase):
    def setUp(self):
        self.calculator = SimpleCalculator()

    def test_ajout_2_int(self):
        S.ajout(2, 3)
        result = S.ajout(2, 3).calcul()
        self.assertEqual(result, 5)
        logging.warning('ok pour test_ajout_2_int')
        logging.info('test_ajout_2_int')

    def test_addtion_integer_string(self):
        S.ajout(2, 3)
        result = S.ajout(2, 3).calcul()
        self.assertEqual(result, "ERROR")

    def test_addition_negative_integers(self):
        S.ajout(2, 3)
        result = S.ajout(2, 3).calcul()
        self.assertEqual(result, -5)

if __name__ == "__main__":
    unittest.main()
